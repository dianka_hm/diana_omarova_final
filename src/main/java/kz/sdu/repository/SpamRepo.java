package kz.sdu.repository;


import kz.sdu.model.SpamFile;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by diana on 5/13/17.
 */
public interface SpamRepo extends CrudRepository<SpamFile, Long> {
}
