package kz.sdu.repository;

import kz.sdu.model.FileM;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by diana on 4/29/17.
 */
public interface FileRepo extends CrudRepository<FileM, Long> {
    Iterable<FileM> findByDate(String date);
}
