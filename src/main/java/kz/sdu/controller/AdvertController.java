package kz.sdu.controller;

import kz.sdu.model.*;
import kz.sdu.model.form.AdvertCreateForm;
import kz.sdu.model.form.AdvertStatus;
import kz.sdu.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

@RestController
public class AdvertController {

    @Autowired
    FileRepo fileRepo;

    @Autowired
    SpamRepo spamRepo;


    @RequestMapping(value = "/file", method = RequestMethod.PUT)
    public ResponseEntity fileUpload(@RequestParam("file") MultipartFile file) throws IOException {
        if (file.getContentType().toLowerCase().equals("text/plain") && file.getSize() < 20480) {
            //System.out.println("file" + file.getContentType() + file.getSize());

            //check for word SPAM
            final Scanner scanner = new Scanner(file.getInputStream());
            while (scanner.hasNextLine()) {
                final String lineFromFile = scanner.nextLine().toLowerCase();
                if (lineFromFile.contains("spam")) {

                    String UPLOAD_DIR_SPAM = "/home/diana/Desktop/FINALWEb/spam/";


                    Path pathFolder = Paths.get(UPLOAD_DIR_SPAM);
                    if (!Files.exists(pathFolder)) {
                        try {
                            Files.createDirectories(pathFolder);
                        } catch (IOException e) {
                            //fail to create directory
                            e.printStackTrace();
                        }
                    }

                    SimpleDateFormat time_formatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss.SSS");
                    String current_time_str = time_formatter.format(System.currentTimeMillis());

                    try {


                        byte[] bytes = file.getBytes();

                        Path path = Paths.get(UPLOAD_DIR_SPAM + current_time_str + file.getOriginalFilename());
                        Files.write(path, bytes);

                        SpamFile spam = new SpamFile();

                        spam.setFilename(current_time_str + file.getOriginalFilename());
                        spam.setFile_path(String.valueOf(path));

                        spamRepo.save(spam);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    System.out.println("SPAAAAAMMMMM");
                    break;
                } else {

                    SimpleDateFormat time_formatter = new SimpleDateFormat("yyyy-MM-dd");
                    String current_time_str = time_formatter.format(System.currentTimeMillis());


                    SimpleDateFormat time_form = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
                    String time_file = time_form.format(System.currentTimeMillis());



                    String UPLOAD_DIR_FILE = "/home/diana/Desktop/FINALWEb/" + current_time_str + "/";

                    Path pathFolder = Paths.get(UPLOAD_DIR_FILE);
                    if (!Files.exists(pathFolder)) {
                        try {
                            Files.createDirectories(pathFolder);
                        } catch (IOException e) {
                            //fail to create directory
                            e.printStackTrace();
                        }
                    }

                    try {

                        byte[] bytes = file.getBytes();

                        Path path = Paths.get(UPLOAD_DIR_FILE + time_file +"_"+ file.getOriginalFilename());
                        Files.write(path, bytes);

                        FileM text = new FileM();

                        text.setFilename(current_time_str+file.getOriginalFilename());
                        text.setFile_path(String.valueOf(path));
                        text.setDate(current_time_str);

                        fileRepo.save(text);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.ACCEPTED);

    }

    @GetMapping(value = "/fileByDate")
    public Iterable<FileM> getAll(@RequestParam("date") String date) {
        return fileRepo.findByDate(date);
    }

    @GetMapping(value = "/listSpam")
    public Iterable<SpamFile> getAllSpam() {
        return spamRepo.findAll();
    }


    @GetMapping(value = "/deleteSpam")
    public String deleteSpam() {
         spamRepo.deleteAll();
         return "Spams deleted";
    }
}
