package kz.sdu.model;

import javax.persistence.*;
import java.nio.file.Path;
import java.util.Date;
import java.util.List;

/**
 * Created by diana on 4/29/17.
 */
@Entity
public class FileM {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String filename;
    private String file_path;
    private String date;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
